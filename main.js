/*
 *   govbot-bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const version = '0.2.0';

var config = { "variables": require('dotenv').config() };

var format = require("string-template");
var rp = require('request-promise');
var when = require('when');
var botName = 'bn-bruecken';
var shared = require('../../modules/sharedFunctions');

var botRoute = '/' + botName;

var botDialog = [
    function(session, args, next) {
        const opendata = require('opendata-bonn');
        const moment = require('moment-timezone');
        moment.locale('de');
        moment.tz.setDefault("Europe/Berlin");

        let result = {};

        opendata
            .GetAktuelleStraßenverkehrslage(opendata.FriedrichEbertBrückeBeideRichtungen)
            .then(function(geojson) {
                geojson.features.forEach(function(element) {
                    if (element.properties.strecke_id == opendata.FriedrichEbertBrückeBonnNachBeuel) {
                        result.text = element.properties.verkehrsstatus;
                        result.auswertezeit = moment
                            .tz(element.properties.auswertezeit,
                                "YYYY-MM-DDThh:mm:ss",
                                "Europe/Berlin");

                        console.log(result);

                        var answerText = generateAnswerTextFromResult(result);

                        session.trans(answerText).then(function(responseTranslated) {
                            var answerJson = generateAnswerJson(session, result, responseTranslated);
                            answerJson = session.toMessage(responseTranslated);
                            session.endDialog(answerJson);
                        });
                    }
                });
            }).catch(function(err) {
                console.log(err);
            });
    }
];

function generateAnswerTextFromResult(result) {
    var answerTextRaw = resolveAnswers[shared.randomWithRange(0, resolveAnswers.length)];

    var answerText = format(answerTextRaw, {
        value: result.text,
        auswertezeit: result.auswertezeit.fromNow(),
    });

    return answerText;
}

function generateAnswerJson(session, result, answerText) {
    return JSON.stringify({
        "botname": botName,
        "type": "text",
        "data": result,
        "text": answerText,
        "language": session.userData.language
    });
}

var resolveAnswers = [
    "Friedrich-Ebert-Brücke: {value} ({auswertezeit})",
    "{value} ... {auswertezeit}",
    '{auswertezeit}: {value}'
];

module.exports = botDialog